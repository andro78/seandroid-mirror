#!/usr/bin/env bash
#
# Copyright (C) 2013 Tresys Technology, LLC
# All Rights Reserved

LOGE() {
	logger -id -t "$TAG/E" "$1"
}

LOGV() {
	if [ -n "$VERBOSE" ]; then
		logger -id -t "$TAG/V" "$1"
	fi
}

purge() {
	local x=$MIRROR_PATH/.repo/local_manifest.xml
	LOGV "Purging $x"
	rm $x
}

clear_stack() {

	dir=`dirs -l -p | tail -n1`
	err=$?
	if [ $err -eq 0 ]; then
		dirs -c
		cd $dir
	fi
}

catch() {

        if [ $2 -ne 0 ]; then
                LOGE "$1 -- $2"
		purge
		clear_stack
                exit $2
        fi
}

sync_local_manifests() {

	if [ ! -d $LOCAL_MANIFESTS_PATH ]; then
		mkdir -p $LOCAL_MANIFESTS_PATH
		LOGV "Initial clone of local manifests"
		git clone $LOCAL_MANIFEST_URL $LOCAL_MANIFESTS_PATH
	else
		LOGV "Fetching local manifests"
		export GIT_DIR=$LOCAL_MANIFESTS_PATH.git
		git fetch --all
		unset GIT_DIR
	fi
}

sync_aosp() {

	LOGV "Start syncing AOSP"
	if [ ! -d $MIRROR_PATH ]; then
		mkdir -p $MIRROR_PATH
		pushd .
		cd $MIRROR_PATH
		LOGV "Initializing repo"
		repo init --mirror -u $AOSP_URL
		popd
	fi
	pushd .
	cd $MIRROR_PATH
	pwd

	repo sync $J
	err=$?
	popd
	catch "Syncing AOSP failed" $err
}


get_aosp_branch() {

	echo $1 | cut -d: -f1-1
}

get_local_manifest_branch() {

	echo $1 | cut -d: -f2-2
}

sync_seandroid() {

	LOGV "Syncing SE4Android"
	pushd .
	cd $MIRROR_PATH
	err=$?
	catch "Could not cd to mirror path $MIRROR_PATH" $?
	for mapping in ${LOCAL_MANIFEST_TO_AOSP[*]}; do
		# get the local manifest branches
		aosp_branch=`get_aosp_branch $mapping`
		local_manifest_branch=`get_local_manifest_branch $mapping`
		LOGV "aosp branch $aosp_branch"
		LOGV "manifest branch $local_manifest_branch"
		pushd .
		cd $LOCAL_MANIFESTS_PATH
		git checkout $local_manifest_branch
		err=$?
		popd
		catch "Checking out local manifests on branch $local_manifest_branch failed" $err
		LOGV "Checked out local manifest branch $local_manifest_branch"
		# Copy the local manifest to the projects .repo dir
		LOGV "cp $LOCAL_MANIFESTS_PATH/local_manifest.xml $MIRROR_PATH/.repo/local_manifest.xml"
		cp $LOCAL_MANIFESTS_PATH/local_manifest.xml $MIRROR_PATH/.repo/local_manifest.xml
		err=$?
		catch "cp of local manifest failed" $err
		LOGV "repo init in CWD: `pwd`"
		# Sync the repo branch with respect to the local manifest branch
		repo init -u $AOSP_URL -b $aosp_branch
		err=$?
		catch "repo init of branch $aosp_branch failed" $err
		repo sync $J
		err=$?
		catch "repo sync of SEAndroid failed" $err
		# Remove the manifest file
		rm $MIRROR_PATH/.repo/local_manifest.xml
		LOGV "rm $MIRROR_PATH/.repo/local_manifest.xml"
		err=$?
		catch "Removing local manifes failed" $err
	done;
	popd
}

do_sync() {

	sync_local_manifests
	sync_aosp
	sync_seandroid
	LOGV "Successfully updated mirror on `date`"
}

usage() {
	prog=$0
	echo "usage: $prog [options]"
	echo "$prog is designed to make initializing and running a mirror of SEAndroid, a tad bit simpler."
	echo "You must have repo installed and in your path!"
	echo "Options:"
	echo "j - Number of threads for repo sync"
	echo "r - The root path to initialize the repo at, defaults to \"\$HOME\""
	echo "s - Whether or not to do the checkout over ssh"
}

while getopts "cdsvj:r:" opt; do
  case $opt in
    d)
      DRYRUN="LOREMIPSUM"
      ;;
    j)
      J="-J$OPTARG"
      ;;
    r)
      ROOTPATH="$OPTARG"
      ;;
    s)
      SSH_BRANCH_SUFFIX="-ssh"
      ;;
    v)
      VERBOSE=1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

if [ -z $J ]; then
	J="-j4"
fi

TAG="SE4Android-mirror"

if [ -z $ROOTPATH ]; then
	ROOTPATH=$HOME
fi

LOCAL_MANIFESTS_PATH=$ROOTPATH/manifests/
MIRROR_PATH=$ROOTPATH/seandroid

AOSP_URL="https://android.googlesource.com/platform/manifest"
LOCAL_MANIFEST_URL="https://bitbucket.org/seandroid/manifests.git"

LOCAL_MANIFEST_TO_AOSP=('android-4.2.2_r1.2b:origin/seandroid-4.2' 'master:origin/master');

if [ -z $J ]; then
	J="-j4"
fi

if [ -n "$VERBOSE" ]; then
	echo "CONFIG"
	echo "ROOTPATH=$ROOTPATH"
	echo "J=$J"
	echo "SSH_BRANCH_SUFFIX=$SSH_BRANCH_SUFFIX"
	echo "LOCAL_MANIFEST_PATH=$LOCAL_MANIFESTS_PATH"
	echo "MIRROR_PATH=$MIRROR_PATH"
	echo "VERBOSE=$VERBOSE"
fi

if [ -z "$DRYRUN" ]; then
	do_sync
fi

exit 0
